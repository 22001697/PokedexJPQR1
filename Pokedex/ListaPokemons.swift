//
//  ListaPokemons.swift
//  Pokedex
//
//  Created by COTEMIG on 26/05/22.
//

import UIKit

class ListaPokemons: UIViewController, UITableViewDataSource {
    private var listaDePokemons = [Pokemon]()
    struct Pokemon {
        var nome: String
        var tipo1: String
        var tipo2 : String?
        var image: String
        var background : String
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self .iniciarPokemons()
        self.tableView.dataSource = self

        // Do any additional setup after loading the view.
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaDePokemons.count
    }
    
    func iniciarPokemons() {
        self.listaDePokemons = [
            Pokemon(nome: "Bubassauro", tipo1: "Grama", tipo2: "Veneno", image: "buba", background: "#48D0B0"),
            Pokemon(nome: "Ivysauro", tipo1: "Grama", tipo2: "Veneno", image: "ivysaur",background: "#48D0B0"),
            Pokemon(nome: "Venussauro", tipo1: "Grama", tipo2: "Veneno", image: "venusaur",background: "#48D0B0"),
            Pokemon(nome: "Charmander", tipo1: "Fogo", tipo2: "", image: "charmander",background: "#FB6C6C"),
            Pokemon(nome: "Charmeleon", tipo1: "Fogo", tipo2: "", image: "charmeleon", background: "#FB6C6C"),
            Pokemon(nome: "Charizard", tipo1: "Fogo", tipo2: "Voador", image: "charizard", background: "#FB6C6C"),
            Pokemon(nome: "Squirtle", tipo1: "Água", tipo2: "", image: "squirtle", background: "#77BDFE"),
            Pokemon(nome: "Wartortle", tipo1: "Água", tipo2: "", image: "wartortle", background: "#77BDFE"),
            Pokemon(nome: "Blastoise", tipo1: "Água", tipo2: "", image: "blastoise", background: "#77BDFE"),
            Pokemon(nome: "Pikachu", tipo1: "Elétrico", tipo2: "", image: "Pikachu", background: "#FFCE4B")
        ]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Pokemon", for: indexPath) as? TableViewCell {
            cell.titulo.text = self.listaDePokemons[indexPath.row].nome
            cell.tipo1.text = self.listaDePokemons[indexPath.row].tipo1
            
            if  self.listaDePokemons[indexPath.row].tipo2 == ""{
                cell.viewTipo2.isHidden = true
            }else{
                cell.viewTipo2.isHidden = false
            }
            cell.tipo2.text = self.listaDePokemons[indexPath.row].tipo2
            cell.pokemonImage.image = UIImage(named: self.listaDePokemons[indexPath.row].image)
            cell.contentView.backgroundColor = hexStringToUIColor(hex: self.listaDePokemons[indexPath.row].background)
            

            return cell
        }
        return UITableViewCell()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
