//
//  TableViewCell.swift
//  Pokedex
//
//  Created by COTEMIG on 26/05/22.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var titulo: UILabel!
    
    @IBOutlet weak var tipo1: UILabel!
    
    @IBOutlet weak var tipo2: UILabel!
    
    @IBOutlet weak var pokemonImage: UIImageView!
    
    @IBOutlet weak var viewTipo2: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
