//
//  ViewController.swift
//  Pokedex
//
//  Created by COTEMIG on 24/02/22.
//

import UIKit

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    


    @IBAction func clickPokedex(_ sender: Any) {
        print("Pokedex")
    }
    
    
    @IBAction func clickHabilidades(_ sender: Any) {
        
        print("Habilidades")
    }
    
    
}

